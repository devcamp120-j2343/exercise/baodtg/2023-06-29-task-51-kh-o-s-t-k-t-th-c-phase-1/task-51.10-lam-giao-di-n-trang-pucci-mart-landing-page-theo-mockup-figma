var gBASE_URL = "https://pucci-mart.onrender.com/api";

onPageLoading()
//function on page loading
function onPageLoading() {
    callApiToGetPetList()
}
//hàm call api lấy danh sách pet
function callApiToGetPetList() {
    $.ajax({
        url: gBASE_URL + "/pets",
        type: "GET",
        success: function (paramRes) {
            console.log(paramRes)
            loadPetCard(paramRes)
        },
        error: function (paramError) {
            console.log(paramError.responseText)
        }
    })
};

//hàm load danh sách vật nuôi lên browser
function loadPetCard(paramPetListObj) {
    $(".div-pet-card").html("")
    for (var bI = 0; bI < paramPetListObj.rows.length; bI++) {
        $(".div-pet-card").append(`
        <div class="card mt-2 mb-3" style="width: 15rem; border: none;">
                    <img class="card-img-top" src="${paramPetListObj.rows[bI].imageUrl}" alt="Card image cap">
                    <div class="card-body text-center" style="height: 100px;">
                        <h5 class="card-title mb-0">${paramPetListObj.rows[bI].name}</h5>
                        <p class="card-text text-secondary mb-0">${paramPetListObj.rows[bI].description}</p>
                        <div class="row justify-content-center">
                            <p class="text-secondary">$${paramPetListObj.rows[bI].price}</p>
                            <p class="ml-2 text-secondary" style="text-decoration:1px line-through black;">$${paramPetListObj.rows[bI].promotionPrice}</p>
                        </div>
                    </div>
                </div>
        `)
    }
}